const fs = require("fs");

// Specify the path to your text file
const filePath = "./test.txt";

// Read the file asynchronously
fs.readFile(filePath, "utf8", (err, data) => {
  if (err) {
    console.error(`Error reading the file: ${err.message}`);
    return;
  }

  console.log(`File content:\n${data}`);
});
