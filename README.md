# readFile - simple node Application to read text form a file and output in console



## To run Application
Run following commands:

Initialize a new Node.js project:

    npm init -y
    
Install the fs module, which is built into Node.js and used for file system operations:

    npm install fs
    
Run your Node.js script:

    node readFile.js
    
## Make sure to check the path of text file in app.js 


